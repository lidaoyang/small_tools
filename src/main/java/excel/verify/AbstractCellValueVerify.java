package excel.verify;

/**
 * 列值校验
 * 
 * @autho 625
 *
 */
public abstract class AbstractCellValueVerify {
	public abstract Object verify(Object fileValue) throws Exception;
}
